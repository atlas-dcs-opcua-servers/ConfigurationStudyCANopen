from lxml import etree, objectify
import re
import os
import pdb
from colorama import Fore, Style

class NodeType():
    def __init__(self, xml_elem):
        self.sdos = {}
        self.sdos = {(x.get('index'),0) : x.attrib for x in xml_elem.xpath('SDOITEM')}
        print(f"Loaded {len(self.sdos)} SDOs for NodeType {xml_elem.get('name')}")

        all_pdos = xml_elem.xpath('RPDO1|RPDO2|RPDO3|RPDO4|TPDO1|TPDO2|TPDO3|TPDO4|NPDO')

        # do we have any segmented SDOs?

        self.has_segmented_sdo = len(xml_elem.xpath("*/SDOITEM[@type='array']")) > 0

        #pdb.set_trace()

        print(all_pdos)

class ConfigFile():
    def __init__(self, fn):
        self.original_entities = []
        entity_re = re.compile(r'<!ENTITY\s+\w+\s+SYSTEM\s+[\'"]([^"]+)[\'"]')
        lines = open(fn, 'r', encoding='utf-8').readlines()
        for ctr, line in enumerate(lines):
            match = entity_re.search(line)
            if match:
                print(match)
                if 'fwElmb/OPCUA_nodeType_ELMB.xml' in match.group(1):
                    print('Detected fwElmb standard ELMB')
                    entity_fn = 'OPCUA_nodeType_ELMB.xml'
                else:
                    self.original_entities.append(match.group(1))
                    entity_fn = os.path.dirname(fn)  + "/nodetypes/" + os.path.basename(match.group(1))
                line = line[:match.start(1)] + entity_fn + line[match.end(1):]
                print(line)
                lines[ctr] = line
        self.fixed_entities_content = ''.join(lines)

    def parse_xml(self):
        self.tree = etree.fromstring(self.fixed_entities_content.encode('utf-8'))
        self.num_buses = len(self.tree.xpath('CANBUS'))
        self.num_nodes = len(self.tree.xpath('*/NODE'))
        self.node_types = list(set([node.get('type') for node in self.tree.xpath('*/NODE')]))
        self.can_ifs = set([node.get('type') for node in self.tree.xpath('CANBUS')])
        self.num_items = len(self.tree.xpath('*/NODE/ITEM'))
        self.min_sync_interval = min([int(interval_s) for interval_s in self.tree.xpath('CANBUS/SYNC/@interval')])
        self.read_nodetypes()

        # check 'orthodox' config: only ITEM within every possible NODE declaration
        node_children = set([x.tag for x in self.tree.xpath('*/NODE/*')])
        orthodox_node_children = {'ITEM', 'atStartup'}
        self.orthodox_nodes_layout = node_children.issubset(orthodox_node_children)
        #print(Fore.GREEN + str(node_children) + Style.RESET_ALL)



    def read_nodetypes(self):
        self.nodetypes = {}
        self.uses_segmented_sdo = False
        nodetypes_elems = self.tree.xpath('NODETYPE')
        for nodetype_elem in nodetypes_elems:
            nodetype = NodeType(nodetype_elem)
            self.nodetypes[nodetype_elem.get('name')] = nodetype
            self.uses_segmented_sdo = self.uses_segmented_sdo or nodetype.has_segmented_sdo

    def compare_nodetypes_against(self, ref_nodetype):
        print('Comparing nodetypes')
        for test_nodetype_name in self.tree.xpath('NODETYPE/@name'):
            print(f'At nodetype {test_nodetype_name}')
            test_nodetype_elems = self.tree.xpath(f"NODETYPE[@name='{test_nodetype_name}']/*")



            ref_etree = etree.parse(ref_nodetype)
