import glob
import re
import os
import pdb
import argparse
import random
import hashlib
from lxml import etree

from ConfigFile import ConfigFile

NSMAP = {None : 'http://cern.ch/quasar/Configuration'}

def transform_pdo3_formula (old_f):
    print('Old formula: ', old_f)
    operand_re = re.compile('(TPDO3.(Value|Flag)_(\d+))')
    while True:
        match = operand_re.search(old_f)
        if match:
            print (match.groups())
            new_var = 'value' if match.groups()[1] == 'Value' else 'adcFlag'
            channel = match.groups()[2]
            old_f = old_f[:match.start(0)]+f'$_.TPDO3.ch{channel}.{new_var}'+old_f[match.end(0):]
        else:
            break
    return old_f

def transform_to_absolute(formula):
    r=re.compile(r'([A-Za-z_][A-Za-z_\d]+)')
    beg=0
    print(f'Started with {formula}')
    while True:
        m = r.search(formula, beg)
        if m:
            formula = formula[:m.start()]+"$_."+formula[m.start():]
            beg=m.start()+4
        else:
            break
    print(f'Ended with with {formula}')
    return formula

def add_model(node_e, model):
    entity = etree.Entity(model)
    entity.tail = '\n      '
    node_e.append(entity)


def convert(config_fn):
    config_file = ConfigFile(config_fn)
    config_file.parse_xml()

    attr_qname = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")

    root = etree.Element('configuration', {attr_qname: 'http://cern.ch/quasar/Configuration ../Configuration/Configuration.xsd'}, nsmap = NSMAP)
    root.append(etree.Element('GlobalSettings', name='GlobalSettings'))

    sim_root = etree.Element('simulation', {etree.QName("http://www.w3.org/2001/XMLSchema-instance", "noNamespaceSchemaLocation"): './CoreSimulator.xsd'})

    for canbus_e in config_file.tree.xpath('CANBUS'):

        sync_e = canbus_e.find('SYNC')
        sync_interval_ms = sync_e.get('interval') if sync_e is not None else 0
        ng_e = canbus_e.find('NODEGUARD')
        ng_e_ms = ng_e.get('interval') if ng_e is not None else 0
        SpeedToSettings = {
            'Unspecified' : 'DontConfigure',
            '100000' : '100k',
            '125000' : '125k'}
        bus_e = etree.Element('Bus',
            name = canbus_e.get('name'),
            syncIntervalMs = str(sync_interval_ms),
            nodeGuardIntervalMs = ng_e_ms,
            port = canbus_e.get('port'),
            provider = canbus_e.get('type'),
            settings = SpeedToSettings[canbus_e.get('speed')]
            )

        sim_bus_e = etree.Element('bus',
            portName = canbus_e.get('port'))

        for node_e in canbus_e.xpath('NODE'):
            new_node_e = etree.Element('Node',
                name = node_e.get('name'),
                id = node_e.get('nodeid'),
                requestedState = 'OPERATIONAL', # that is to be discussed
                stateInfoSource = 'NodeGuard' # this is to be made conditional
                )
            new_node_e.text = '\n      '

            add_model(new_node_e, 'STDELMB_FOUNDATIONS')
            add_model(new_node_e, 'STDELMB_DI_TPDO1_C') # this is difficult to judge w/o having DPLs ...

            if len(node_e.xpath('ITEM')) > 0: # use this model only if people have ITEMs
                add_model(new_node_e, 'STDELMB_AI_TPDO3')

            sim_serial_before_hash = bus_e.get('name') + str(new_node_e.get('id'))
            sim_serial_hashed = hashlib.md5(sim_serial_before_hash.encode('utf-8')).digest()
            sim_serial_number = int.from_bytes(sim_serial_hashed[:4], 'little') % 1000

            sim_node_e = etree.Element('node',
                type = 'elmb',
                id = node_e.get('nodeid'),
                sdoDelay = '0.01',
                serial=f'X{sim_serial_number:03}')

            for item_e in node_e.xpath('ITEM'):
                #if 'DewPoint' in item_e.get('name'):
                #    print(f"Skipping {item_e.get('name')}")
                #    #continue # TODO: quick hack!
                #print(item_e)
                value_formula = transform_pdo3_formula(item_e.get('value'))
                #value_formula = transform_to_absolute(value_formula)
                cv_e = etree.Element('CalculatedVariable', name=item_e.get('name'), value=value_formula)
                if 'status' in item_e.attrib:
                    cv_e.attrib['status'] = transform_pdo3_formula(item_e.get('status'))
                # pdb.set_trace()
                cv_e.tail = '\n      '
                new_node_e.append(cv_e)

            #pdb.set_trace()
            new_node_e.getchildren()[-1].tail = '\n    '
            bus_e.append(new_node_e)
            sim_bus_e.append(sim_node_e)

        root.append(bus_e)
        sim_root.append(sim_bus_e)

    models = ['STDELMB_FOUNDATIONS', 'STDELMB_DI_TPDO1_C', 'STDELMB_AI_TPDO3']

    dt = "<!DOCTYPE configuration [\n" + \
         '\n'.join(f'  <!ENTITY {model} SYSTEM "CANopen_def_{model}.xmle">' for model in models) + \
         "\n]>\n"

#<!ENTITY ELMB SYSTEM "NODETYPE_ELMB.xml">''' +

    converted_conf_fn = os.path.dirname(config_fn) + '/ng_config.xml'
    fo = open(converted_conf_fn, 'wb')
    fo.write(etree.tostring(root, doctype=dt, pretty_print=True, xml_declaration=True))

    fsim = open(os.path.dirname(config_fn) + '/sim_config.xml', 'wb')
    fsim.write(etree.tostring(sim_root, pretty_print=True, xml_declaration=True))

    print(f'File was written as {converted_conf_fn}')

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--file', required=False)
    args = parser.parse_args()

    if args.file:
        convert(args.file)
    else:
        print('Batch mode not available yet')

    #
    # config_fns = glob.glob('../data/*/*/OPCUACANOpenServer.xml')
    #
    # fn_re = re.compile(r'data/(\w+)/(\w+)/OPCUACANOpenServer.xml')
    #
    # for config_fn in config_fns:
    #     match = fn_re.search(config_fn)
    #     print(match)
    #     experiment, system = match.groups()
    #
    #     config_file = ConfigFile(config_fn)
    #     config_file.parse_xml()






if __name__ == "__main__":
    main()
