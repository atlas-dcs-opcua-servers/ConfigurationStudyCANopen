import glob
import re

from yattag import Doc, indent

EmbeddedCss = """
    body
    {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-width: 10px;
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid #bbb;
        padding: 4px;
    }
    th
    {
        background-color: #4CAF50;
        color: white;
        text-align: left;
        padding-top: 12px;
        padding-bottom: 12px;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }
    .tr_phase
    {
        background-color: #bce3be;
        text-align: center;
        font-style: italic;
    }
    """

def main():
    config_fns = glob.glob('../data/*/*/OPCUACANOpenServer.xml')

    fn_re = re.compile(r'data/(\w+)/(\w+)/OPCUACANOpenServer.xml')

    doc, tag, text, line = Doc().ttl()
    with tag('html'):
        with tag('head'):
            with tag('title'): text("Summary of known quasar-based OPC-UA servers")
            doc.asis(f'<style>{EmbeddedCss}</style>\n\n')
            doc.asis('<script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>')
        with tag('table', klass='sortable'):
            with tag('thead'):
                with tag('tr'):
                    with tag('th'): text('Experiment')
                    with tag('th'): text('System')
                    #with tag('th'): text('optional modules (non-exhaustive)')
                    #with tag('th'): text('dflt endpoint')
            for config_fn in config_fns:
                match = fn_re.search(config_fn)
                print(match)
                experiment, system = match.groups()
                with tag('tr'):
                    with tag('td'): text(experiment)
                    with tag('td'): text(system)


    out_file = open('list_systems.html','w')
    out_file.write(indent(doc.getvalue()))
    out_file.close()



if __name__ == "__main__":
    main()
