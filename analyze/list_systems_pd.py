import glob
import re
import os
import pdb

from yattag import Doc, indent

import pandas as pd

from ConfigFile import ConfigFile

EmbeddedCss = """
    body
    {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-width: 1px;
        border-collapse: collapse;
    }
    td, th {
        border: 2px solid #ffffff;
        padding: 4px;
    }
    th
    {
        background-color: #4CAF50;
        color: white;
        text-align: left;
        padding-top: 3px;
        padding-bottom: 3px;
    }
    td
    {
        max-width: 20em;
    }
    tr:nth-child(even){
        background-color: #f2f2f2;
    }

    """

def empty_if_zero(value):
    return '' if value == 0 else str(value)

def main():
    config_fns = glob.glob('../data/*/*/OPCUACANOpenServer.xml')

    fn_re = re.compile(r'data/(\w+)/(\w+)/OPCUACANOpenServer.xml')

    table = {}

    for config_fn in config_fns:
        match = fn_re.search(config_fn)
        print(match)
        experiment, system = match.groups()

        keys = ['experiment', 'node_types', '#buses', '#nodes', 'CAN_ifs', 'num_items', 'min_sync_interval', 'orthodox_node_layout', 'error']
        info = {key:'' for key in keys}
        info['experiment'] = experiment

        try:
            config_file = ConfigFile(config_fn)
            config_file.parse_xml()
            info['#buses'] = int(str(config_file.num_buses))
            info['#nodes'] = str(config_file.num_nodes)
            node_types = ', '.join(sorted(config_file.node_types))
            info['node_types'] = node_types if len(node_types) < 50 else '-- list too long --'
            info['CAN_ifs'] = ','.join(config_file.can_ifs)
            info['num_items'] = empty_if_zero(config_file.num_items)
            info['min_sync_interval'] = empty_if_zero(config_file.min_sync_interval)
            info['orthodox_node_layout'] = '✓' if config_file.orthodox_nodes_layout else ''
            info['segm_sdo'] = '✓' if config_file.uses_segmented_sdo else ''
            config_file.compare_nodetypes_against('OPCUA_nodeType_ELMB.xml')
        except Exception as ex:
            info['error'] = str(ex)
        table[system] = info



    df = pd.DataFrame.from_dict(table, orient='index', dtype=str)
    df.sort_index(inplace=True)
    #df=df.append(df.sum(numeric_only=True), ignore_index=True)
    #df.loc['Total']= df.sum(numeric_only=True)
    #df.append('TOTAL')
    print(df)



    df.to_csv('list_systems_pd.csv')
    df.to_latex('list_systems_pd.tex', longtable=True, multirow=True, columns=['experiment', 'CAN_ifs', 'node_types', '#buses', '#nodes', 'num_items'],
        caption="Quick reference data from performed analysis. num\_items corresponds to a~number of calculated items.", label="tbl:config_acq")

    doc, tag, text, line = Doc().ttl()
    with tag('html'):
        with tag('head'):
            with tag('title'): text("CANopen configuration study")
            doc.asis(f'<style>{EmbeddedCss}</style>\n\n')
            doc.asis('<script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>')
        with tag('body'):
            doc.asis(df.to_html())

    out_file = open('list_systems.html','w')
    out_file.write(indent(doc.getvalue()))
    out_file.close()






if __name__ == "__main__":
    main()
