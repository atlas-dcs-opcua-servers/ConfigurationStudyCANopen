import argparse
import glob
import re
import os
import shutil
import sys
from ConfigFile import ConfigFile

def yes_or_no(question):
    '''A simple user interaction asking for yes or no.'''
    while True:
        print(question+' type y or n; then enter   ')
        sys.stdout.flush()
        yn = input()
        if yn in ['y','n']:
            return yn

def verbose_mkdir(path, doit):
    if doit:
        print(f'Creating directory {path}')
        try:
            os.mkdir(path)
        except FileExistsError:
            pass
    else:
        print(f'Would create {path}')            

def verbose_copy(from_, to, doit):
    if doit:
        print(f'Copying {from_} -> {to}')
        try:
            shutil.copy(from_, to)
        except Exception as ex:
            print(f'Failed to copy {from_}')
    else:
        print(f'Would copy {from_} -> {to}')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--root', required=True)
    parser.add_argument('--do_it', action="store_true")
    parser.add_argument('--select_system', type=str, default=None)

    args = parser.parse_args()

    config_file_names = glob.glob(f'{args.root}/ATLAS_DCS*/*/config/OPCUACANOpenServer.xml', recursive=True)

    file_path_re = re.compile(r'.*ATLAS_DCS_(\w+)/(\w+)/config/.*')

    for config_fn in config_file_names:

        match = file_path_re.match(config_fn)
        if match:
            system = match.groups()[1]
            if args.select_system and system != args.select_system:
                continue 
            print(config_fn)
            target_dir = f'{os.getcwd()}/../data/ATLAS/{system}'
            # see if we have nodetypes
            cf = ConfigFile(config_fn)
            for entity in cf.original_entities:
                print(f'Recognized entity {entity}')
                if not os.path.isabs(entity):
                    entity = os.path.join(os.path.dirname(config_fn), entity) 
                target_dir_nodetypes = f'{target_dir}/nodetypes' 
                verbose_mkdir(target_dir_nodetypes, args.do_it)
                verbose_copy(entity, target_dir_nodetypes, args.do_it)
            if not os.path.isdir(target_dir):
                verbose_mkdir(target_dir, args.do_it)
            verbose_copy(config_fn, target_dir, args.do_it)

if __name__ == '__main__':
    main()
